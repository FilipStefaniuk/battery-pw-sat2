#ifndef BATTERY_H
#define BATTERY_H
#include <QObject>

class Battery : public QObject
{
    Q_OBJECT
public:
    Battery();
    void change(double a, double b);
    double value;
    double capacity;
};

#endif // BATTERY_H
