#-------------------------------------------------
#
# Project created by QtCreator 2014-04-06T21:48:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Battery_Simulator
TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    battery.cpp

HEADERS  += mainwindow.h \
    battery.h

FORMS    += mainwindow.ui
