#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "battery.h"
#include "QTimer"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    timer(new QTimer(this))
{
    ui->setupUi(this);
    ui->lcdNumber->display(ui->doubleSpinBox->value());
    ui->progressBar->setValue((int)(ui->doubleSpinBox->value() * 100 / ui->doubleSpinBox_2->value()));
    connect(timer, SIGNAL(timeout()), this, SLOT(loop()));
    connect(ui->doubleSpinBox, SIGNAL(valueChanged(double)), ui->lcdNumber, SLOT(display(double)));
    battery.capacity = ui->doubleSpinBox_2->value();
    battery.value = ui->doubleSpinBox->value();
}
void MainWindow::loop()
{
    battery.change(ui->doubleSpinBox_3->value(),ui->doubleSpinBox_4->value());
    ui->lcdNumber->display(battery.value);
    ui->progressBar->setValue((int)(battery.value * 100 / ui->doubleSpinBox_2->value()));
}
MainWindow::~MainWindow()
{
    if (timer != nullptr)
        delete timer;
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->doubleSpinBox->setEnabled(false);
    timer->start(1000/ui->spinBox->value());
    battery.capacity = ui->doubleSpinBox_2->value();
    battery.value = ui->doubleSpinBox->value();
}

void MainWindow::on_pushButton_2_clicked()
{
    timer->stop();
    ui->doubleSpinBox->setEnabled(true);
    ui->doubleSpinBox->setValue(battery.value);
}
